import nfc
import time
from threading import Thread
import binascii

dgi5001 = bytearray.fromhex("6F3A840E325041592E5359532E4444463031A528BF0C2561234F0E325041592E5359532E44444630318701019F120D5441312E30312E5050312E3030")
dgi5002 = bytearray.fromhex("771682025B80941008010100100101011801020020010200")
dgi5003 = bytearray.fromhex("7781899F2701409F360219699F4B601D38D33F515ABF0609AD859464897BF5EE5CC6B2F5FE00B9F02583BA0704C6AC6E6374C708D944011C31A9E4A3F032310620787C10F88B37AA0CB61BAF827BB2D545DF1BA8DA490F4115D817674382EC55185AC786359FFC94748683E46D197B9F101A22109040030400010000000010469799FFFF00000000000000FF")
dgi5004 = bytearray.fromhex("0102030405060708090a0b0c0d0e0f101112131415161718191a1b1c1d1e1f202122232425262728292a2b2c2d2e2f303132333435363738393a3b3c3d3e3f404142434445464748494a4b4c4d4e4f505152535455565758595a5b5c5d5e5f606162636465666768696a6b6c6d6e6f707172737475767778797a7b7c7d7e7f808182838485868788898a8b8c8d8e8f909192939495969798999a9b9c9d9e9fa0a1a2a3a4a5a6a7a8a9aaabacadaeafb0b1b2b3b4b5b6b7b8b9babbbcbdbebfc0c1c2c3c4c5c6c7c8c9cacbcccdcecfd0d1d2d3d4d5d6d7d8d9dadbdcdddedfe0e1e2e3e4e5e6e7e8e9eaebecedeeeff0f1f2f3f4f5f6f7f8f9fafbfcfdfeff")
dgi5005 = bytearray.fromhex("fffefdfcfbfaf9f8f7f6f5f4f3f2f1f0efeeedecebeae9e8e7e6e5e4e3e2e1e0dfdedddcdbdad9d8d7d6d5d4d3d2d1d0cfcecdcccbcac9c8c7c6c5c4c3c2c1c0bfbebdbcbbbab9b8b7b6b5b4b3b2b1b0afaeadacabaaa9a8a7a6a5a4a3a2a1a09f9e9d9c9b9a999897969594939291908f8e8d8c8b8a898887868584838281807f7e7d7c7b7a797877767574737271706f6e6d6c6b6a696867666564636261605f5e5d5c5b5a595857565554535251504f4e4d4c4b4a494847464544434241403f3e3d3c3b3a393837363534333231302f2e2d2c2b2a292827262524232221201f1e1d1c1b1a191817161514131211100f0e0d0c0b0a090807060504030201")
dgi5006 = bytearray.fromhex("002b")

def selectPPSE(tag):
    print("EMVCo Tests")
    print(tag)
    data = bytearray.fromhex("325041592E5359532E4444463031")
    rspn = tag.send_apdu(0x00, 0xA4, 0x04, 0x00,data, check_status=False)
    print("Response: DGI 5001")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))
    # print(binascii.hexlify(bytearray(rspn)))
    # print(binascii.hexlify(bytearray(dgi5001)))
    if(rspn == dgi5001):
        print("DGI 5001 PASS")
    else:
        print ("DGI 5001 FAIL")
        print(binascii.hexlify(bytearray(rspn)))
        print(binascii.hexlify(bytearray(dgi5001)))
    print (40*'*')

    time.sleep(0.1)
    data = bytearray.fromhex("0000")
    rspn = tag.send_apdu(0x80, 0xA8, 0x00, 0x00,data, check_status=False)
    print("Response: DGI 5002")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))
    # print(binascii.hexlify(bytearray(rspn)))
    # print(binascii.hexlify(bytearray(dgi5001)))
    if(rspn == dgi5002):
        print("DGI 5002 PASS")
    else:
        print ("DGI 5002 FAIL")
        print(binascii.hexlify(bytearray(rspn)))
        print(binascii.hexlify(bytearray(dgi5002)))
    print (40*'*')

    time.sleep(0.1)
    data = bytearray.fromhex("0000")
    rspn = tag.send_apdu(0x80, 0xAE, 0x00, 0x00,data, check_status=False)
    print("Response: DGI 5003")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))
    # print(binascii.hexlify(bytearray(rspn)))
    # print(binascii.hexlify(bytearray(dgi5001)))
    if(rspn == dgi5003):
        print("DGI 5003 PASS")
    else:
        print ("DGI 5003 FAIL")
        print(binascii.hexlify(bytearray(rspn)))
        print(binascii.hexlify(bytearray(dgi5003)))
    print (40*'*')


    time.sleep(0.1)
    data = bytearray.fromhex("0000")
    rspn = tag.send_apdu(0x00, 0x36, 0x00, 0x00,data, check_status=False)
    print("Response: SW1SW2 6300")
    print(rspn)
    print(binascii.hexlify(bytearray(rspn)))
    print (40*'*')

    time.sleep(0.1)
    data = bytearray.fromhex("")    
    rspn = tag.send_apdu(0x00, 0xCA, 0xDF, 0x01,data, check_status=False)
    print("Response: application version")
    print(rspn)
    print(binascii.hexlify(bytearray(rspn)))
    print (40*'*')

    time.sleep(0.1)
    data = bytearray.fromhex("")    
    rspn = tag.send_apdu(0x00, 0xCA, 0xDF, 0x02,data, check_status=False)
    print("Response: DGI 5006")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))
    # print(binascii.hexlify(bytearray(rspn)))
    # print(binascii.hexlify(bytearray(dgi5001)))
    if(rspn == dgi5006):
        print("DGI 5006 PASS")
    else:
        print ("DGI 5006 FAIL")
        print(binascii.hexlify(bytearray(rspn)))
        print(binascii.hexlify(bytearray(dgi5006)))
    print (40*'*')

    time.sleep(0.1)
    data = bytearray.fromhex("")    
    rspn = tag.send_apdu(0x00, 0xCA, 0xDF, 0x02,data, check_status=False)
    print("Response: DGI 5007")
    print(rspn)
    print(binascii.hexlify(bytearray(rspn)))
    print (40*'*')

    time.sleep(0.1)
    data = bytearray.fromhex("0000")
    rspn = tag.send_apdu(0x00, 0x46, 0x00, 0x00,data, check_status=False)
    print("Response: SW1SW2 6400")
    print(rspn)
    print(binascii.hexlify(bytearray(rspn)))
    print (40*'*')

    time.sleep(0.1)
    data = bytearray.fromhex("")    
    rspn = tag.send_apdu(0x00, 0x02, 0xDF, 0x02,data, check_status=False)
    print("Response: DGI 5004")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))
    # print(binascii.hexlify(bytearray(rspn)))
    # print(binascii.hexlify(bytearray(dgi5001)))
    if(rspn == dgi5004):
        print("DGI 5004 PASS")
    else:
        print ("DGI 5004 FAIL")
        print(binascii.hexlify(bytearray(rspn)))
        print(binascii.hexlify(bytearray(dgi5004)))
    print (40*'*')


    time.sleep(0.1)
    data = bytearray.fromhex("")    
    rspn = tag.send_apdu(0x00, 0x04, 0xDF, 0x02,data, check_status=False)
    print("Response: DGI 5005")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))
    # print(binascii.hexlify(bytearray(rspn)))
    # print(binascii.hexlify(bytearray(dgi5001)))
    if(rspn == dgi5005):
        print("DGI 5005 PASS")
    else:
        print ("DGI 5005 FAIL")
        print(binascii.hexlify(bytearray(rspn)))
        print(binascii.hexlify(bytearray(dgi5005)))

    print (40*'*')





def on_startup(targets):
    print("startup")
    return targets

def on_connect(tag):
    print(tag)
    print ("connected")
    Thread(target=selectPPSE, args=(tag,)).start()
    return True

def on_release(tag):
    print('Released')
    return tag

rdwr_options = {
    'on-startup': on_startup,
    'on-connect': on_connect,
    'on-release': on_release,
    'beep-on-connect': False,
}

with nfc.ContactlessFrontend('usb') as clf:
    tag = clf.connect(rdwr=rdwr_options)