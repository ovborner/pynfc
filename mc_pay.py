import nfc
import time
from threading import Thread
import binascii


def selectPPSE(tag):
    print("MSD Card Payment")
    print(tag)
    print("Select PPSE")
    data = bytearray.fromhex("325041592E5359532E4444463031") #"2Pay.SYS.DDF01"
    print(data)
    rspn = tag.send_apdu(0x00, 0xA4, 0x04, 0x00,data, check_status=False)
    print("Select PPSE Response: ")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print(binascii.hexlify(bytearray(rspn)))
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))

    print(80*'*')

    print("Select AID")
    data = bytearray.fromhex("A0000000041010") #"select 0x041010"
    print(data)
    rspn = tag.send_apdu(0x00, 0xA4, 0x04, 0x00,data, check_status=False)
    print("Select AID Response: ")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print(binascii.hexlify(bytearray(rspn)))
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))

    print(80*'*')

    print("GPO")    
    data = bytearray.fromhex("028300") #GPO?
    print(data)
    rspn = tag.send_apdu(0x80, 0xA8, 0x00, 0x00,data, check_status=False)
    print("GPO Response: ")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print(binascii.hexlify(bytearray(rspn)))
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))
    
    print(80*'*')
    print("Read Record - B2")    
    data = bytearray.fromhex("0100") #Read Record?
    print(data)
    rspn = tag.send_apdu(0x00, 0xB2, 0x01, 0x0C,data, check_status=False)
    print("Read Record Response: ")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print(binascii.hexlify(bytearray(rspn)))
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))

    print(80*'*')
    print("CRYPTOGRAPHIC CHECKSUM:")    
    data = bytearray.fromhex("0400000187") #CC
    print(data)
    print(binascii.hexlify(bytearray(data)))
    rspn = tag.send_apdu(0x80, 0x2A, 0x8E, 0x80,data, check_status=False)
    print("Crypto Response: ")
    swCode = rspn[-2:]
    rspn = rspn[:-2]
    print(binascii.hexlify(bytearray(rspn)))
    print("response SW1SW2")
    print(binascii.hexlify(bytearray(swCode)))

    print(80*'*')
    print(80*'*')
    print(80*'*')
    


     







def on_startup(targets):
    print("startup")
    return targets

def on_connect(tag):
    print(tag)
    print ("connected")
    Thread(target=selectPPSE, args=(tag,)).start()
    return True

def on_release(tag):
    print('Released')
    return tag

rdwr_options = {
    'on-startup': on_startup,
    'on-connect': on_connect,
    'on-release': on_release,
    'beep-on-connect': False,
}

with nfc.ContactlessFrontend('usb') as clf:
    tag = clf.connect(rdwr=rdwr_options)